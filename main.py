import GameOfLife as GoF


def main():
    print('''
If you want to use the console, enter 1
If you want to use input.txt, enter 2''')
    inputID = input()
    print('Enter the number of generations')
    if inputID == '1':
        generations = int(input())
    elif inputID == '2':
        f = open('input.txt', 'r')
        generations = int(f.readline())

    print('Enter Ocean size Y X')
    if inputID == '1':
        y, x = map(int, input().split())
    elif inputID == '2':
        y, x = map(int, f.readline().split())

    print('''
Fill the Ocean:
. - Nothing
# - Rock
F - Fish
C - Crayfish
''')

    G = GoF.Game(x, y)
    if inputID == '1':
        G.OceanInput(input)
    elif inputID == '2':
        G.OceanInput(f.readline)
    G.PlayGame(generations)

    print('''
If you want to use the console, type 1
If you want to use output.txt, enter 2''')
    outputID = input()
    if outputID == '1':
        G.OceanOutput(print)
    if outputID == '2':
        g = open('output.txt', 'w')
        G.OceanOutput(g.write)
        g.close()


print("It's a Game of Life.")
main()
