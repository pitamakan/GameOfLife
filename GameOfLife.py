import copy


class Factory:
    def create(self, name, x, y):
        if (name == 'F'):
            return Fish(x, y)
        elif (name == '#'):
            return Rock(x, y)
        elif (name == '.'):
            return Nothing(x, y)
        elif (name == 'C'):
            return Crayfish(x, y)
        else:
            raise NotImplementedError()


class Unit(object):
    def __init__(self, xCoord, yCoord):
        raise NotImplementedError()

    def MakeChanges(self):
        raise NotImplementedError()


class Fish(Unit):
    def __init__(self, xCoord, yCoord):
        self.x, self.y = xCoord, yCoord
        self.sign = 'F'

    def MakeChanges(self, game):
        return game.WillDie(self.x, self.y)


class Rock(Unit):
    def __init__(self, xCoord, yCoord):
        self.x, self.y = xCoord, yCoord
        self.sign = '#'

    def MakeChanges(self, game):
        return self


class Crayfish(Unit):
    def __init__(self, xCoord, yCoord):
        self.x, self.y = xCoord, yCoord
        self.sign = 'C'

    def MakeChanges(self, game):
        return game.WillDie(self.x, self.y)


class Nothing(Unit):
    def __init__(self, xCoord, yCoord):
        self.x, self.y = xCoord, yCoord
        self.sign = '.'

    def MakeChanges(self, game):
        return game.WillBorn(self.x, self.y)


class Game:
    def __init__(self, x, y):
        self.xSize = x
        self.ySize = y
        self.MainOcean = list()
        self.SecondOcean = list()

    def OceanInput(self, Input):
        F = Factory()
        for i in range(self.ySize):
            self.SecondOcean.append([])
            for j in range(self.xSize):
                self.SecondOcean[i].append(Nothing(i, j))
        Start = list()
        for i in range(self.ySize):
            Start.append(Input())
        for i in range(self.ySize):
            self.MainOcean.append([])
            for j in range(self.xSize):
                self.MainOcean[i].append(F.create(Start[i][j], j, i))

    def OceanOutput(self, output):
        for i in self.MainOcean:
            out = ''
            for j in i:
                out += j.sign
            output(out)
            output('\n')

    def PlayGame(self, GenNum):
        for i in range(GenNum):
            for y in range(self.ySize):
                for x in range(self.xSize):
                    self.SecondOcean[y][x] = \
                        self.MainOcean[y][x].MakeChanges(self)
            self.MainOcean = copy.deepcopy(self.SecondOcean)

    def WillBorn(self, x, y):
        CrayfishCount = 0
        FishCount = 0
        for i in MOVE:
            if (self.isCorrect([x + i[0], y + i[1]])):
                a = type(self.MainOcean[y+i[1]][x+i[0]])
                if (a == Fish):
                    FishCount += 1
                elif (a == Crayfish):
                    CrayfishCount += 1
        if (FishCount >= 3):
            return Fish(x, y)
        elif (CrayfishCount >= 3):
            return Crayfish(x, y)
        else:
            return Nothing(x, y)

    def WillDie(self, x, y):
        neighbors = 0
        for i in MOVE:
            if (self.isCorrect([x + i[0], y + i[1]])):
                a = type(self.MainOcean[y+i[1]][x+i[0]])
                b = type(self.MainOcean[y][x])
                if (a == b):
                    neighbors += 1
        if (neighbors > 3 or neighbors <= 1):
            return Nothing(x, y)
        else:
            return self.MainOcean[y][x]

    def isCorrect(self, couple):
        xCorrect = (len(self.MainOcean[0]) > couple[0] >= 0)
        yCorrect = (len(self.MainOcean) > couple[1] >= 0)
        return (xCorrect and yCorrect)


MOVE = [[1, 0], [0, 1], [1, 1], [-1, 0], [0, -1], [-1, -1], [-1, 1], [1, -1]]
