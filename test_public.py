import GameOfLife as GoF
import unittest
import os
import numpy as np


class TestFactory(unittest.TestCase):
    def test_factory(self):
        Fac = GoF.Factory()
        x = 5
        y = 7

        F = Fac.create('F', x, y)
        self.assertTrue(isinstance(F, GoF.Fish) and
                        F.x == x and F.y == y and
                        F.sign == 'F')

        R = Fac.create('#', x, y)
        self.assertTrue(isinstance(R, GoF.Rock) and
                        R.x == x and R.y == y and
                        R.sign == '#')

        N = Fac.create('.', x, y)
        self.assertTrue(isinstance(N, GoF.Nothing) and
                        N.x == x and N.y == y and
                        N.sign == '.')

        C = Fac.create('C', x, y)
        self.assertTrue(isinstance(C, GoF.Crayfish) and
                        C.x == x and C.y == y and
                        C.sign == 'C')


class TestOcean(unittest.TestCase):
    def test_ocean(self):
        n = 3
        m = 5
        G = GoF.Game(n, m)
        G.MainOcean = [[0] * m for i in range(n)]
        ok = True
        for i in range(-3, n+3):
            for j in range(-3, m+3):
                if ((-1 < i < m) and (-1 < j < n)
                        and not G.isCorrect((i, j))):
                    ok = False
                    break
                if (not(-1 < i < m) and not(-1 < j < n)
                        and G.isCorrect((i, j))):
                    ok = False
                    break
        self.assertTrue(ok)


class TestBorn(unittest.TestCase):
    def will_born_test(self):
        n = 5
        m = 3
        coord = {
            (1, 1): 'Crayfish',
            (3, 1): 'Crayfish'
        }
        filename = 'test/WillBorn.txt'
        TESTDATA_FILENAME = os.path.join(os.path.dirname(__file__), filename)
        G = GoF.Game(n, m)
        file = open(TESTDATA_FILENAME, 'r')
        G.OceanInput(file.readline)
        ok = True
        for i in range(m):
            for j in range(n):
                now = G.WillBorn(i, j)
                if (isinstance(now, GoF.Nothing)
                        and (i, j) in coord):
                    ok = False
                if ((i, j) in coord and
                        not isinstance(now, getattr(GoF, coord[(i, j)]))):
                    ok = False
        self.assertTrue(ok)


class TestDie(unittest.TestCase):
    def test_will_die(self):
        n = 5
        m = 3
        coord = {(0, 2), (2, 2)}
        filename = 'test/WillDie.txt'
        TESTDATA_FILENAME = os.path.join(os.path.dirname(__file__), filename)
        G = GoF.Game(n, m)
        file = open(TESTDATA_FILENAME, 'r')
        G.OceanInput(file.readline)
        ok = True
        for i in range(m):
            for j in range(n):
                now = G.WillDie(j, i)
                if (isinstance(now, GoF.Nothing)
                        and not ((i, j) in coord)
                        and not isinstance(now, type(G.MainOcean[i][j]))):
                    ok = False
                if ((i, j) in coord and
                        not isinstance(now, GoF.Nothing)):
                    ok = False
        self.assertTrue(ok)


class TestGlobal(unittest.TestCase):
    def test_main(self):
        for i in range(1, 6):
            in_filename = 'test/test' + str(i) + '.txt'
            out_filename = 'answer/test' + str(i) + '.txt'
        IN_FILENAME = os.path.join(os.path.dirname(__file__), in_filename)
        OUT_FILENAME = os.path.join(os.path.dirname(__file__), out_filename)
        input = open(IN_FILENAME, 'r')
        output = open(OUT_FILENAME, 'r')
        generations = int(input.readline())
        y, x = map(int, input.readline().split())

        G = GoF.Game(x, y)
        G.OceanInput(input.readline)
        G.PlayGame(generations)

        OUTPUT = os.path.join(os.path.dirname(__file__), 'output.txt')
        g = open(OUTPUT, 'w')
        G.OceanOutput(g.write)
        g.close()

        g = open(OUTPUT, 'r')
        self.assertTrue(g.readlines() == output.readlines())


class TestSum(unittest.TestCase):
    def test_factory(self):
        self.assertTrue(3, np.sum([0, 1, 2]))


if __name__ == '__main__':
    unittest.main()
